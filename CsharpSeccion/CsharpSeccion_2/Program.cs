﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsharpSeccion_2.Classes;

namespace CsharpSeccion_2
{
    class Program
    {
        static IOperations am;
        static void Main(string[] args)
        {
            //Console.WriteLine("simple math");
            //Console.WriteLine(SimpleMath.Division(430.23f,54000.2f));

            //am = new AdvancedMath();
            //Console.WriteLine(am.Remainder(7,3));

            //BankAccount bankAccount1 = new BankAccount(1000.50f,"jane doe");
            //ChildBankAccount bankAccount2 = new ChildBankAccount(1000.50f, "jhon doe", "jane doe");

            //Console.WriteLine("la propiedad");
            //Console.WriteLine(bankAccount1.Balance);

            //Console.WriteLine("add balance normal");
            //Console.WriteLine(bankAccount1.AddBalance(100f));

            //Console.WriteLine("Cuenta de niños y add balance");
            //Console.WriteLine(bankAccount2.AddBalance(-9f,true));

            GetData();
            Console.ReadLine();
        }
        async static void GetData()
        {
            BankAccount bankAccount = new BankAccount(543543,"Jane");
            Console.WriteLine("log in");
            var task =await bankAccount.GetData();
            Console.WriteLine(task);
        }
    }

    interface IOperations
    {
        float Remainder(float dividend,float divisor);
    }

    class SimpleMath
    {
        public static float Add(float n1,float n2)
        {
            return n1 + n2;
        }
        public static float Substract(float n1, float n2)
        {
            return n1 - n2;
        }
        public static float Multiplication(float n1, float n2)
        {
            return n1 * n2;
        }
        public static float Division(float n1, float n2)
        {
            return n1 / n2;
        }
    }
    class AdvancedMath : SimpleMath, IOperations
    {
        public float Remainder(float dividend, float divisor)
        {
            return dividend % divisor;
        }
    }
}

